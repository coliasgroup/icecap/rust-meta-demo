capdl_script_config_json_hash := $(shell printf '%s' "$(capdl_script_config_json_content)" | sha256sum | cut -d' ' -f1)
capdl_script_config_json := $(instance_build_dir)/cdl-config.$(capdl_script_config_json_hash).json

$(capdl_script_config_json):
	printf %s '$(capdl_script_config_json_content)' > $@

capdl_spec_dir := $(instance_build_dir)/cdl
capdl_spec_intermediate := $(capdl_spec_dir).intermediate
capdl_spec := $(capdl_spec_dir)/icecap.cdl
capdl_spec_fill_dir := $(capdl_spec_dir)/links

$(capdl_spec): $(capdl_spec_intermediate) ;

INTERMEDITE: $(capdl_spec_intermediate)
$(capdl_spec_intermediate): \
		$(capdl_script_config_json) \
		$(min_component_binaries) $(full_component_binaries) \
		$(object_sizes_yaml) \
		$(serialize_runtime_config_tool) \
		$(serialize_example_component_config_tool)
	PATH=$(common_tools_dir):$$PATH \
	PYTHONPATH=$(icecap_python_src):$(capdl_python_src):$$PYTHONPATH \
	CONFIG=$(capdl_script_config_json) \
	OUT_DIR=$(capdl_spec_dir) \
		python3 $(capdl_script)

capdl_spec_json := $(instance_build_dir)/spec.json

$(capdl_spec_json): $(capdl_tool) $(object_sizes) $(capdl_spec)
	$(capdl_tool) --code-dynamic-alloc --object-sizes=$(object_sizes_yaml) --json=$@ $(capdl_spec)

###

root_task_crate_name := capdl-loader
root_task_dir := $(instance_build_dir)/root-task
root_task := $(root_task_dir)/$(root_task_crate_name).elf
root_task_intermediate := $(root_task).intermediate

$(root_task_for_payload): $(root_task)
	install -D $< $@

$(root_task): $(root_task_intermediate) ;

.INTERMEDITE: $(root_task_intermediate)
$(root_task_intermediate): $(capdl_spec_json) $(capdl_spec_fill_dir)
	cd $(rust_src) && \
		$(icecap_cargo_env) \
		CAPDL_SPEC_FILE=$(abspath $(capdl_spec_json)) \
		CAPDL_FILL_DIR=$(abspath $(capdl_spec_fill_dir)) \
        ICECAP_RUNTIME_ROOT_HEAP_SIZE=0 \
        ICECAP_RUNTIME_ROOT_STACK_SIZE=$(shell expr 4096 '*' 16) \
			cargo build \
				$(icecap_cargo_flags) \
				--profile release-capdl-loader \
				--out-dir $(abspath $(root_task_dir)) \
				-p $(root_task_crate_name)
