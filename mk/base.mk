dir_without_trailing_slash = $(patsubst %/,%,$(dir $(1)))

instance_makefile_path := $(abspath $(firstword $(MAKEFILE_LIST)))
instance_path := $(call dir_without_trailing_slash,$(instance_makefile_path))
base_mk_path := $(abspath $(lastword $(MAKEFILE_LIST)))
root := $(call dir_without_trailing_slash,$(call dir_without_trailing_slash,$(base_mk_path)))
abstract_instance_path := $(shell realpath --relative-to $(root) $(instance_path))

rust_src := $(root)/src

icecap_src := $(root)/icecap/src
icecap_rust_src := $(icecap_src)/rust
icecap_python_src := $(icecap_src)/python
capdl_src := $(root)/capdl
capdl_python_src := $(capdl_src)/python-capdl-tool
capdl_tool_src := $(capdl_src)/capDL-tool

rust_target_path := $(icecap_rust_src)/support/targets

sel4_quickstart_bundle := $(root)/seL4-quickstart-bundle/out
kernel_config_json := $(sel4_quickstart_bundle)/sel4-aux/config.json
platform_info_yaml := $(sel4_quickstart_bundle)/sel4-aux/platform_gen.yaml

build_dir := $(root)/tmp
common_build_dir := $(build_dir)/common
common_rust_build_dir := $(common_build_dir)/target
common_tools_dir := $(common_build_dir)/tools
instance_build_dir := $(build_dir)/$(abstract_instance_path)
instance_rust_build_dir := $(instance_build_dir)/target
instance_tools_dir := $(instance_build_dir)/tools

capdl_tool_in_tree := $(capdl_tool_src)/parse-capDL
capdl_tool := $(common_build_dir)/parse-capDL
capdl_tool_intermediate := $(capdl_tool).intermediate

object_sizes_yaml := $(common_build_dir)/object_sizes.yaml

root_task_for_payload := $(instance_build_dir)/root-task.elf
payload_cpio := $(instance_build_dir)/payload.cpio
rendered_loader := $(instance_build_dir)/rendered-loader.elf

icecap_cargo_env := \
	RUST_TARGET_PATH=$(abspath $(rust_target_path)) \
	SEL4_CONFIG=$(abspath $(kernel_config_json)) \
	SEL4_PLATFORM_INFO=$(abspath $(platform_info_yaml)) \
	LIBSEL4_INCLUDE_DIRS="$(abspath $(sel4_quickstart_bundle))/include" \
	LIBSEL4_LIB_DIR="$(abspath $(sel4_quickstart_bundle))/lib"

icecap_build_std_flags := \
	-Z build-std=core,alloc,compiler_builtins \
	-Z build-std-features=compiler-builtins-mem

icecap_cargo_flags := \
	-Z unstable-options \
	$(icecap_build_std_flags) \
	--target aarch64-icecap \
	--target-dir $(abspath $(instance_rust_build_dir))

qemu_command_prefix = \
	qemu-system-aarch64 \
		-machine virt,virtualization=on,highmem=off,secure=off,$(1) \
		-cpu cortex-a57 \
		-smp 2 \
		-m size=1024 \
		-nographic

###

define declare_tool # $(1) <- scope, $(2) <- name, $(3) <- crate

$(2) := $$($(1)_tools_dir)/$(3)
$(2)_intermediate := $$($(2)).intermediate

$$($(2)): $$($(2)_intermediate) ;

.INTERMEDITE: $$($(2)_intermediate)
$$($(2)_intermediate):
	cd $$(rust_src) && \
		cargo build \
			-Z unstable-options \
			--target-dir $$(abspath $$($(1)_rust_build_dir)) \
			--out-dir $$(abspath $$($(1)_tools_dir)) \
			-p $(3)
endef

###

.PHONY: none
none: ;

.PHONY: clean-all
clean-all:
	rm -rf $(build_dir)

.PHONY: clean
clean:
	rm -rf $(instance_build_dir)

.PHONY: build
build: $(rendered_loader)

.PHONY: run
run: $(rendered_loader)
	$(call qemu_command_prefix,) \
		-semihosting-config enable=on,target=native \
		-kernel $<

###

$(eval $(call declare_tool,common,render_elf_tool,render-elf-with-data))
$(eval $(call declare_tool,common,serialize_runtime_config_tool,icecap-serialize-runtime-config))

###

$(capdl_tool): $(capdl_tool_in_tree)
	install -D $< $@

$(capdl_tool_in_tree): $(capdl_tool_intermediate) ;

$(capdl_tool_intermediate):
	$(MAKE) -C $(capdl_tool_src)

$(object_sizes_yaml): $(capdl_src)/object_sizes/object_sizes.yaml
	aarch64-linux-gnu-gcc -E -P \
		-I $(sel4_quickstart_bundle)/include \
		- < $< > $@

###

payload_cpio_dir := $(payload_cpio).d
payload_cpio_file_names := kernel.elf kernel.dtb app.elf
payload_cpio_file_paths := $(addprefix $(payload_cpio_dir)/,$(payload_cpio_file_names))

$(payload_cpio): $(payload_cpio_file_paths)
	echo $(payload_cpio_file_names) | tr ' ' '\n' | cpio -D $(payload_cpio_dir) -o -H newc > $@

$(payload_cpio_dir)/%: $(sel4_quickstart_bundle)/boot/%
	install -D $< $@

$(payload_cpio_dir)/app.elf: $(root_task_for_payload)
	install -D $< $@

$(rendered_loader): $(render_elf_tool) $(payload_cpio)
	$(render_elf_tool) $(sel4_quickstart_bundle)/boot/loader.elf \
		--inject $(payload_cpio),start=deferred_payload_start,end=deferred_payload_end \
		--record-image-bounds start=deferred_image_start,end=deferred_image_end \
		-o $@
