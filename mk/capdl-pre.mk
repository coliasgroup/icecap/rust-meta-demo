components_dir := $(instance_build_dir)/components
full_component_binaries_dir := $(components_dir)/full
full_component_binaries := $(addprefix $(full_component_binaries_dir)/,$(component_file_names))
min_component_binaries_dir := $(components_dir)/min
min_component_binaries := $(addprefix $(min_component_binaries_dir)/,$(component_file_names))

$(min_component_binaries_dir):
	mkdir -p $@

$(min_component_binaries_dir)/%.elf: $(full_component_binaries_dir)/%.elf | $(min_component_binaries_dir)
	aarch64-linux-gnu-strip -s $< -o $@

config_json_common := \
	"plat": "virt", \
	"object_sizes": "$(abspath $(object_sizes_yaml))", \
	"platform_info": "$(abspath $(platform_info_yaml))",

mk_component_image_entry = "image": { \
	"full": "$(abspath $(full_component_binaries_dir)/$(1).elf)", \
	"min": "$(abspath $(min_component_binaries_dir)/$(1).elf)" \
}
