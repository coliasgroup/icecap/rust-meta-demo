# Demo of building and simulating a Rust userspace for seL4 against pre-built seL4 artifacts

See [seL4-quickstart-bundle/out](seL4-quickstart-bundle/out) for the prebuilt artifacts.

See [seL4-quickstart-bundle/seL4-bundle/tools/seL4](https://gitlab.com/coliasgroup/icecap/seL4_tools/-/tree/bundle) for the patches to `seL4_tools` which enable a CPIO payload to be provided to the elfloader at run-time rather than embedded at build-time.

See [Makefile](Makefile) for how it all fits together.

## Build instructions

Building and running this demo requires Make and Docker.

First, clone this respository and its submodules:

```
git clone --recursive https://gitlab.com/coliasgroup/icecap/rust-meta-demo.git
cd rust-meta-demo
```

Next, build, run, and enter a Docker container for development:

```
make -C docker run && make -C docker exec
```

Now, inside the container, build and simulate a simple system:

```
# inside the container
make run
```
