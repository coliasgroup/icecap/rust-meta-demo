cmake_minimum_required(VERSION 3.7.2)

set(ARM_CPU "cortex-a57" CACHE STRING "" FORCE)

set(KernelArch arm CACHE STRING "" FORCE)
set(KernelSel4Arch aarch64 CACHE STRING "" FORCE)
set(KernelPlatform qemu-arm-virt CACHE STRING "" FORCE)
set(KernelArmHypervisorSupport ON CACHE BOOL "" FORCE)
set(KernelVerificationBuild OFF CACHE BOOL "" FORCE)
set(KernelDebugBuild ON CACHE BOOL "" FORCE)
set(KernelMaxNumNodes 2 CACHE STRING "" FORCE)
set(ElfloaderDeferLinkingPayload ON CACHE BOOL "" FORCE)
set(LibSel4FunctionAttributes public CACHE STRING "" FORCE)
