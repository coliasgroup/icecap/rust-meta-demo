examples := $(sort $(wildcard examples/*))

.PHONY: all
all:
	for example in $(examples); do \
		$(MAKE) -C $$example build; \
	done
